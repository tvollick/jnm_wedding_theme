<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php 
// Top left Menu 
$argsLeft = array(
	'theme_location' => 'top_left', 
	'container'       => 'div',
	'container_class' => 'top-menu-panel',
	'container_id'    => 'top-left-menu',
	'menu_class'      => 'menu',
	'menu_id'         => '',
	'echo'            => true,
	'fallback_cb'     => 'wp_page_menu',
	'before'          => '',
	'after'           => '',
	'link_before'     => '',
	'link_after'      => '',
	'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
	'depth'           => 0,
	'walker'          => ''
); 
$argsRight = array(
	'theme_location' => 'top_right', 
	'container'       => 'div',
	'container_class' => 'top-menu-panel',
	'container_id'    => 'top-left-menu',
	'menu_class'      => 'menu',
	'menu_id'         => '',
	'echo'            => true,
	'fallback_cb'     => 'wp_page_menu',
	'before'          => '',
	'after'           => '',
	'link_before'     => '',
	'link_after'      => '',
	'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
	'depth'           => 0,
	'walker'          => ''
); 
$argsMobile = array(
	'theme_location' => 'mobile', 
	'container'       => 'div',
	'container_class' => 'mobile-menu',
	'container_id'    => 'mobile-menu',
	'menu_class'      => 'menu',
	'menu_id'         => '',
	'echo'            => true,
	'fallback_cb'     => 'wp_page_menu',
	'before'          => '',
	'after'           => '',
	'link_before'     => '',
	'link_after'      => '',
	'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
	'depth'           => 0,
	'walker'          => ''
); 
?> 

<header>
	<div class="nav-container">
		<div class="top-left">
				<?php wp_nav_menu($argsLeft); ?>
		</div>
		<div class="site-title"><span> Jesse + Markie </span> </div>
		<div class="top-right">
				<?php wp_nav_menu($argsRight); ?>
		</div>
		<div id="mobile-menu-container">
			<?php wp_nav_menu($argsMobile); ?> 
		</div> 
		<div id="mobile-toggle">
			<div id="toggle-icon"></div>
		</div>
	</div>
</header>
<div id="rsvp">
			<?php if (is_active_sidebar('rsvp_widget_area')) { ?>
		<div id="rsvp-container">
			<div id="exit">x
			</div>
			<?php dynamic_sidebar('rsvp_widget_area'); ?> 
		</div>
			<?php } // end if 	?>




</div>
<div id="page" class="hfeed site">
	
	<!-- Where I removed Sidebar --> 

	<div id="content" class="site-content">
