<?php
function theme_enqueue_styles() {

    $parent_style = 'parent-style';

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    // wp_enqueue_style( 'child-style',
    //     get_stylesheet_directory_uri() . '/style.css',
    //     array( $parent_style )
    // );
    wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css?family=Josefin+Sans:400,700' ); 
    wp_enqueue_style('fullpageCSS', get_stylesheet_directory_uri(). '/includes/fullpage/jquery.fullpage.css'); 


    wp_enqueue_style('featherlightCSS', '//cdn.rawgit.com/noelboss/featherlight/1.3.5/release/featherlight.min.css');
    wp_enqueue_script('slimScroll', get_stylesheet_directory_uri(). '/includes/fullpage/jquery.slimscroll.min.js', array('jquery'), false, true); 
    wp_enqueue_script('fullpageJs', get_stylesheet_directory_uri(). '/includes/fullpage/jquery.fullpage.js', array('jquery'), false, true); 
    wp_enqueue_script('extraJS', get_stylesheet_directory_uri(). '/includes/js/extra.js', array('jquery'), false, true ); 
    wp_enqueue_script('rsvpJS', get_stylesheet_directory_uri(). '/includes/js/rsvp.js', array('jquery'), false, true ); 
    wp_enqueue_script('countDown', get_stylesheet_directory_uri(). '/includes/js/countdown.js', array('jquery'), false, true ); 
    wp_enqueue_script('featherlight', '//cdn.rawgit.com/noelboss/featherlight/1.3.5/release/featherlight.min.js', array('jquery'), false, true ); 
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

function get_post_thumbnail_url ($current_post) {
	if (has_post_thumbnail($current_post->ID)) {
		$thumbnail_id = get_post_thumbnail_id($current_post->ID); 
		$thumbnail = wp_get_attachment_image_src($thumbnail_id, "full"); 
		$thumbnail_url = $thumbnail[0]; 
		return $thumbnail_url; 
	}; 
}

register_nav_menus(array(
	"top_left" => "Top Left Menu", 
	"top_right" => "Top Right Menu", 
    "mobile" => "Mobile"
)); 

function rsvp_widget_area () {
    register_sidebar(array(
        'name' => 'RSVP Sidebar', 
        'id' => 'rsvp_widget_area', 
        'before_widget' => '<div id="rsvp_widget_area">', 
        'after_widget' => '</div>' 
    )); 
}
add_action('widgets_init', 'rsvp_widget_area'); 




?>