(function($){
	$(document).ready(function(){


		// var weddingDate = '2016-6-5'; 
		var weddingDate = 'June 5 2016 13:00:00 GMT-06:00'; 
		initializeClock('clockdiv', weddingDate); 


	}); 

	function getTimeRemaining(endtime) {
		var t = Date.parse(endtime) - Date.parse(new Date()); 
		var seconds = Math.floor((t/1000) % 60); 
		var minutes = Math.floor((t/1000/60) % 60); 
		var hours = Math.floor((t/(1000*60*60)) % 24); 
		var days = Math.floor(t/(1000*60*60*24)); 
		return {
			'total': t, 
			'days' : days, 
			'hours': hours, 
			'minutes': minutes, 
			'seconds': seconds
		}; 
	}

	function initializeClock(id, endtime){
		var clock = document.getElementById(id); 
		console.log(clock); 
		var daysSpan = clock.querySelector('.days'),
				hoursSpan = clock.querySelector('.hours'), 
				minutesSpan = clock.querySelector('.minutes'),
				secondsSpan = clock.querySelector('.seconds');

		function updateClock () {
			var t = getTimeRemaining(endtime); 
			daysSpan.innerHTML = t.days;
	    hoursSpan.innerHTML = t.hours;
	    minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
	    secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);			

			if(t.total <= 0) {
				clearInterval(timeinterval); 
			}			
		}

		updateClock(); 

		var timeinterval = setInterval(updateClock, 1000); 


	}


})(jQuery); 