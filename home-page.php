<?php
/**
 * Template Name: Jesse and Markie
 */


get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main home-page-templatea" role="main">
			<div id="fullpage">


			<?php
			// Hero Section. Use's home page post content 
			while ( have_posts() ) {
				the_post(); 
				global $post; 
				?> 
				<section id="hero" class="dark-bg section" style="background-image: url(<?php echo get_post_thumbnail_url($post);?> );">
					<div class="hero-contents section-container">
						<h1 class="hero-title white"><?php the_title(); ?></h1>
						<h3 class="hero-subtitle white "><?php the_content(); ?></h3>
						<span id="rsvp-button" class="rsvp-button rsvp">RSVP</span>
					</div> 
				</section>

				<?php 
			} // End Loop 
			wp_reset_query(); 

			// Our Story Section
			$wp_query = new WP_Query(array('post_type'=>'page','name'=>'our-story')); 
			if ($wp_query->have_posts()) {
				while($wp_query->have_posts()){
					$wp_query->the_post(); 
				?>
				<section id="our-story " class="section" >
					<div class="section-container">
						<h1 class="dark"><?php the_title(); ?></h1>
						<h2 class="color-1 subtitle"></h2>
						<div class="section-content"><?php the_content(); ?></div>
					</div>
				</section>
				<?php

				}
			}
			wp_reset_query(); 

			// Save The Date Section
			$wp_query = new WP_Query(array('post_type' => 'page', 'name' => 'save-the-date')); 
			if ($wp_query->have_posts()) {
				while($wp_query->have_posts()) {
					$wp_query->the_post(); 
			?>
				<section id="save-the-date" class="section" style="background-image: url(<?php echo get_post_thumbnail_url($post);?> );">
					<div class="section-container">
						<h3 class="white"><?php the_title();?></h3>
						<h1 class="white"> June 5, 2016 </h1>
					</div>
				</section>
			<?php 
				}
			}
			wp_reset_query(); 

			// Wedding Details Section 
			$wp_query = new WP_Query(array('post_type' => 'page', 'name' => 'wedding-details')); 
			if ($wp_query->have_posts()) {
				while($wp_query->have_posts()) {
					$wp_query->the_post(); 
			?>
				<section id="wedding-details" class="section">
					<div class="section-container">
						<h1><?php the_title(); ?> </h1>
						<div class="section-content"><?php the_content(); ?></div>
					</div>
				</section>
			<?php 
				}
			}
			wp_reset_query(); 

			// Accomodations Section 
			$wp_query = new WP_Query(array('post_type' => 'page', 'name' => 'accomodations')); 
			if ($wp_query->have_posts()) {
				while($wp_query->have_posts()) {
					$wp_query->the_post(); 
			?>
				<section id="accomodations" class="section">
					<div class="section-container">
						<h1><?php the_title(); ?> </h1>
						<div class="section-content"><?php the_content(); ?></div>
					</div>
				</section>
			<?php 
				}
			}
			wp_reset_query(); 

			// Photos Section
			$wp_query = new WP_Query(array('post_type' => 'page', 'name' => 'photos')); 
			if ($wp_query->have_posts()) {
				while($wp_query->have_posts()) {
					$wp_query->the_post(); 
			?>
				<section id="photos" class="section">
					<div class="section-container">
						<h1 class="white"><?php the_title(); ?> </h1>
						<div class="section-content"><?php the_content(); ?></div>
					</div>
				</section>
			<?php 
				}
			}
			wp_reset_query(); 

			// Registry Section 
			$wp_query = new WP_Query(array('post_type' => 'page', 'name' => 'registry')); 
			if ($wp_query->have_posts()) {
				while($wp_query->have_posts()) {
					$wp_query->the_post(); 
			?>
				<section id="registry" class="section">
					<div class="section-container">
						<h1><?php the_title(); ?> </h1>
						<div class="section-content clearfix"><?php the_content(); ?></div>
					</div>
				</section>
			<?php 
				}
			}
			wp_reset_query(); 

			// Footer 
			$wp_query = new WP_Query(array('post_type' => 'page', 'name' => 'countdown')); 
			if ($wp_query->have_posts()) {
				while($wp_query->have_posts()) {
					$wp_query->the_post(); 
			?>
				<section id="countdown" class="section" style="background-image: url(<?php echo get_post_thumbnail_url($post);?> );">
					<div class="section-container">
						<div class="section-content">
							<div id="clockdiv">
								<div id="days" class="time-group">
									Days<br>
							    <span class="days"></span>
								</div>
								<div id="hours" class="time-group">
									Hours<br>
							    <span class="hours"></span>
								</div>
								<div id="minutes" class="time-group">
									Minutes<br>
							    <span class="minutes"></span>
								</div>
								<div id="seconds" class="time-group">
									Seconds<br> 
							    <span class="seconds"></span>
								</div>
							</div>							
						</div>
					</div>
				</section>
			<?php 
				}
			}
			wp_reset_query(); 

			?>
			</div>
		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
